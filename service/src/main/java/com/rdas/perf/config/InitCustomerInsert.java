package com.rdas.perf.config;

import com.rdas.perf.model.Customer;
import com.rdas.perf.repository.CustomerRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Log4j2
@Component
public class InitCustomerInsert implements CommandLineRunner {

    @Autowired
    DataSource dataSource;

    @Autowired
    CustomerRepository customerRepository;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Transactional(readOnly = true)
    @Override
    public void run(String... args) throws Exception {
        log.info("DATASOURCE = {} \n Begin to Insert CUSTOMERS.!!" , dataSource);
        insertCustomers();
        log.info("Finished insert Customers!");

        log.info("\n1.findAll()...");
        for (Customer customer : customerRepository.findAll()) {
            log.info("\t ** From find all, customer {}",customer);
        }

        log.info("\n2.findByEmail(String email)...");
        for (Customer customer : customerRepository.findByEmail("222@yahoo.com")) {
            log.info(customer);
        }

        log.info("\n3.findByDate(Date date)...");
        for (Customer customer : customerRepository.findByDate(sdf.parse("2017-02-12"))) {
            log.info(customer);
        }

        // For Stream, need @Transactional
        log.info("\n4.findByEmailReturnStream(@Param(\"email\") String email)...");
        try (Stream<Customer> stream = customerRepository.findByEmailReturnStream("333@yahoo.com")) {
            stream.forEach(x -> log.info(" Streamed Customer {}",x) );
        }
        log.info("Done!");
    }

    private void insertCustomers() {
        IntStream.range(0, 30000).parallel().forEach(
                nbr -> {
                    try {
                        Customer c = createRand(nbr);
                        customerRepository.save(c);
                        Thread.sleep(1);
                    } catch (InterruptedException ex) {
                    }
                    log.info(nbr);
                }
        );
    }

    private Customer createRand(int counter) {
        Customer c = new Customer();
//        c.setId();
        c.setName("xxx"+counter);
        c.setEmail(counter+"name@email.com");
        return c;
    }
}
