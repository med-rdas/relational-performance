package com.rdas.perf.service;

import com.rdas.perf.model.Player;
import com.rdas.perf.model.Team;
import com.rdas.perf.repository.PlayerRepository;
import com.rdas.perf.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class SoccerServiceImpl implements SoccerService {

    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private TeamRepository teamRepository;

    @Override
    public List<String> getAllTeamPlayers(long teamId) {
        List<String> result = new ArrayList<>();
        List<Player> players = playerRepository.findByTeamId(teamId);
        for (Player player : players) {
            result.add(player.getName());
        }

        return result;
    }

    @Override
    public void addBarcelonaPlayer(String name, String position, int number) {

        Team barcelona = teamRepository.findByPlayers(1l);

        Player newPlayer = new Player();
        newPlayer.setName(name);
        newPlayer.setPosition(position);
        newPlayer.setNum(number);
        newPlayer.setTeam(barcelona);
        playerRepository.save(newPlayer);
    }
}
